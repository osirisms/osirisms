from net.swordie.ms.constants import BossConstants
import time

while not sm.zakumAlreadySpawned(sm.getFieldID()):
    drop = sm.getDropInRect(BossConstants.ZAKUM_CHAOS_SPAWN_ITEM, BossConstants.ZAKUM_RECT)



    if drop is not None and drop.getItem().getQuantity() == 1:

        time.sleep(3.75)
        field.removeDrop(drop.getObjectId(), 0, False, -1)
        sm.spawnZakum(sm.getFieldID())

        #Dispose to not allow any more Zakums to be spawned, or persist

        sm.dispose();