from net.swordie.ms.constants import BossConstants
import time

drop = sm.getDropInRect(BossConstants.ZAKUM_EASY_SPAWN_ITEM, BossConstants.ZAKUM_RECT)

if drop is not None and drop.getItem().getQuantity() == 1:
    time.sleep(3.75)
    field.removeDrop(drop.getObjectId(), 0, False, -1)
    sm.spawnZakum(sm.getFieldID())

    #Dispose to not allow any more Zakums to be spawned, or persist

    sm.dispose();