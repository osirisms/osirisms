 # Sunstone Grave (9201071) | MesoGears: Fire Chamber (600020400)
 # Author: Tiger
 # sm.sendSayOkay("Tempt Fate. Discover the path.")

Pets = [5000000, 5000001, 5000002, 5000003, 5000004, 5000005, 5000006, 5000007, 5000008, 5000009, 5000010, 5000012, 5000014, 5000017, 5000020, 5000030, 5000031, 5000032, 5000033, 5000049, 5000060]
SpellTrace = 4001832
listStr = "Please Choose a Pet:\r\n"

selection = sm.sendNext("Hello. How can I help you Tester? #b\r\n"
            "#L0#Please Give Me a Pet#l\r\n"
            "#L1#I Need Spell Traces#l\r\n")

if selection == 0:
  i = 0
  while i < len(Pets):
   listStr += "#L" + str(i) + "##i" + str(Pets[i]) + "# "
   i += 1
  selection1 = sm.sendNext(listStr)
  if selection1 >= 0:
   if sm.canHold(Pets[selection1], 1):
    sm.giveItem(Pets[selection1], 1)
    sm.sendSayOkay("Here You Go!")
   else:
    sm.sendSayOkay("Please make room in your Cash Inventory.")
if selection == 1:
 selection1 = sm.sendAskNumber("How many do you need tester?", 1, 1, 5000)
 sm.giveItem(SpellTrace, selection1)
 sm.sendSayOkay("Here you go " + str(selection1) + " Spell Traces")

sm.dispose()