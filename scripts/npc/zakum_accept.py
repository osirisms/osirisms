# Adobis - Entrance To (Easy/Chaos) Zakum's Altar
fields = {
    211042402 : 280030200, # Easy
    211042400 : 280030100, # Normal
    211042401 : 280030000 # Chaos
}
response = sm.sendAskYesNo("Are you sure you are prepared to visit Zakum's Altar?")


if response:
    if sm.getParty() is None:
        sm.sendSayOkay("You must create a party before attempting to challenge Zakum.")
    elif not sm.isPartyLeader():
        sm.sendSayOkay("Please have your party leader talk to me if you wish to face Zakum.")
    elif sm.checkParty():
        sm.setPartyDeathCount(20)
        sm.warpInstanceIn(fields[sm.getFieldID()], True)
        sm.invokeAfterDelay(10000, "getDropInRect", 4001017, 200)