# Deet (1033104) | Frozen Fairy Forest : Elluel  |  Part of Mercedes Intro Questline

Pets = [5000000, 5000002, 5000008,  5000012, 5000017, 5000033, 5000049]
SpellTrace = 4001832
listStr = ""

selection = sm.sendNext("Hello. How can I help you Tester? #b\r\n"
            "#L0#Please Give Me a Pet#l\r\n"
            "#L1#I Need Spell Traces#l\r\n")

if selection == 0:
  i = 0
  while i < len(Pets):
   listStr += "\r\n#L" + str(i) + "##i" + str(Pets[i]) + "# #z" + str(Pets[i]) + "# "
   i += 1
  selection1 = sm.sendNext(listStr)
  if selection1 >= 0:
   if sm.canHold(Pets[selection1], 1):
    sm.giveItem(Pets[selection1], 1)
    sm.sendSayOkay("Here You Go!")
   else:
    sm.sendSayOkay("Please make room in your Cash Inventory.")
if selection == 1:
 selection1 = sm.sendAskNumber(materialStr, 1, 1, 5000)
 sm.giveItem(SpellTrace, selection1)
 sm.sendSayOkay("Here you go " + str(selection1) + " Spell Traces")

sm.dispose()