from net.swordie.ms.constants import BossConstants

# Adobis - Door to Zakum field

response = sm.sendNext("Well... Okay. It seems you fulfill the requirements. What would you like to do?\r\n\r\n#L0##bRecieve an offering for Zakum.\r\n#L1##bGo to El Nath.")

if response == 0:

	response1 = sm.sendNext("Which difficulty of Zakum are you making an offering to?\r\n#L0##bEasy Zakum\r\n#L1##bNormal/Chaos Zakum")

	if response1 == 0:

		itemCount1 = sm.sendAskNumber("How many #rEasy Zakum #koffering stones would you like to obtain?", 5, 1, 200)

		if sm.canHold(BossConstants.ZAKUM_EASY_SPAWN_ITEM, itemCount1):

			sm.giveItem(BossConstants.ZAKUM_EASY_SPAWN_ITEM, itemCount1)

	else:

		itemCount2 = sm.sendAskNumber("How many #rNormal/Chaos Zakum #koffering stones would you like to obtain?", 5, 1, 200)

		if sm.canHold(BossConstants.ZAKUM_HARD_SPAWN_ITEM, itemCount2):

			sm.giveItem(BossConstants.ZAKUM_HARD_SPAWN_ITEM, itemCount2)

if response == 1:

	response2 = sm.sendAskYesNo("Are you sure you want to be transported El Nath?")

	if response2 == 1:

		sm.warp(211000000)

	else:

		sm.sendSayOkay("That's a shame! Remember if you need a transport back to El Nath, I can ensure your safe return!")