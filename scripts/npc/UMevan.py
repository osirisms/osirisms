# 1105009 : EVAN - Starter Map - Job Advancer | @evan
from net.swordie.ms.constants import JobConstants
if not JobConstants.isEvan(sm.getChr().getJob()):
    sm.dispose()
    if sm.getFieldID() == 100030102:
        sm.sendNext("Hallo #h #, and Welcome to #e#bOsirisMs#n#k.\r\nPlease Take the time to read this guide for Evan Advancement.\r\n\r\nWhenever you get the desired level for advancing,\r\nyou can use the #r@evan#k command to open the advancement npc.\r\nThis will allow you to advance to the next phase of Evan.")
    if chr.getJob() == 2001:
        if sm.getChr().getLevel() < 10:
            sm.sendSayOkay("Your not yet level 10.")
            sm.dispose()
        else:
            response = sm.sendAskYesNo("Would you like to make the job advancement?")
            if response:
                sm.jobAdvance(2210)
                sm.sendSayOkay("Congratulations on making the job advancement!")
                sm.dispose()
    if chr.getJob() == 2210:
        if sm.getChr().getLevel() < 30:
            sm.sendSayOkay("Your not yet level 30.")
            sm.dispose()
        else:
            response = sm.sendAskYesNo("Would you like to make the job advancement?")
            if response:
                sm.jobAdvance(2212)
                sm.sendSayOkay("Congratulations on making the job advancement!")
                sm.dispose()
    if chr.getJob() == 2212:
        if sm.getChr().getLevel() < 60:
            sm.sendSayOkay("Your not yet level 60.")
            sm.dispose()
        else:
            response = sm.sendAskYesNo("Would you like to make the job advancement?")
            if response:
                sm.jobAdvance(2214)
                sm.sendSayOkay("Congratulations on making the job advancement!")
                sm.dispose()
    if chr.getJob() == 2214:
        if sm.getChr().getLevel() < 100:
            sm.sendSayOkay("Your not yet level 100.")
            sm.dispose()
        else:
            response = sm.sendAskYesNo("Would you like to make the job advancement?")
            if response:
                sm.jobAdvance(2218)
                sm.sendSayOkay("Congratulations on making the job advancement!")
                sm.dispose()